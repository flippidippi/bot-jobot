// Description:
//   Just different stuff
//
// Configuration:
//   None
//
// Commands:
//   hubot clear - jobot licks the screen clean with newlines
//   hubot yn X - jobot gives you the answer to a yes/no question X
//
// Author:
//   flippidippi

function miscellaneous (robot) {
  // Clears the screen with jobot licks
  robot.respond(/clear$/i, msg => {
    msg.send(':tongue:\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n:tongue:')
  })

  // Returns yes or no question with a Gif
  robot.respond(/yn .+$/i, msg => {
    msg.http('https://yesno.wtf/api').get()((err, response, body) => {
      if (!err) {
        msg.send(JSON.parse(body).image)
      }
    })
  })

  // Returns bird up
  robot.respond(/bird up$/i, msg => {
    msg.send(':bird: :bird-on-em: :bird-person: :flappy_bird: :kiwi-bird: :conga_party_parrot: :parrot_dad: :conga_parrot: :parrot: :parrot_beer: :parrot_cop: :parrot_mustache: :parrot_sleep: :parrothd: :reverse_conga_parrot: :fast_parrot: :ship_it_parrot: :aussie_conga_parrot: :aussie_parrot: :banana_parrot: :blonde_sassy_parrot: :blues_clues_parrot: :bored_parrot: :chill_parrot: :christmas_parrot: :coffee_parrot: :confused_parrot: :dark_beer_parrot: :deal_with_it_parrot: :donut_parrot: :dreidel_parrot: :explody_parrot: :fieri_parrot: :fiesta_parrot: :gentleman_parrot: :goth_parrot: :halal_parrot: :hamburger_parrot: :harry_potter_parrot: :ice_cream_parrot: :john_francis_parrot: :love_parrot: :margarita_parrot: :mexa_parrot: :middle_parrot: :moonwalking_parrot: :nyan_parrot: :old_timey_parrot: :orioles_parrot: :party_parrot: :pizza_parrot: :popcorn_parrot: :reverse_conga_parrot: :right_parrot: :sad_parrot: :sassy_parrot: :party_trash_dove: :dove_of_peace:')
  })
}

module.exports = miscellaneous
