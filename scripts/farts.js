// Description:
//   Keeps count of farts
//
// Configuration:
//   FIREBASE_PROJECT_ID
//   FIREBASE_CLIENT_EMAIL
//   FIREBASE_PRIVATE_KEY
//   FIREBASE_DATABASE_URL
//
// Commands:
//   hubot farts - displays the current number of farts
//   farts+X - increases fart counter by X (0-9)
//   farts-X - decreases fart counter by X (0-9)
//
// Author:
//   flippidippi

const database = require('../lib/firebase-database')()
const db = database()

function farts (robot) {
  // Get farts
  robot.respond(/farts$/i, async msg => {
    const fartCount = (await db.ref('farts/count').once('value')).val()

    msg.send(`:clap: *${fartCount}* farts :dash:`)
  })

  // Add farts
  robot.hear(/(?:^|\s)farts\+(\d)(?:$|\s)/i, async msg => {
    const fartCount = (await db.ref('farts/count').once('value')).val()
    const count = +msg.match[1]
    const total = +fartCount + count

    db.ref('farts').update({ count: total })

    msg.send(`:upvote: *${total}* farts :dash:`)
  })

  // Remove farts
  robot.hear(/(?:^|\s)farts-(\d)(?:$|\s)/i, async msg => {
    const fartCount = (await db.ref('farts/count').once('value')).val()
    let count = +msg.match[1]
    const total = +fartCount - count

    if (count < 0) {
      count = 0
    }

    db.ref('farts').update({ count: total })

    msg.send(`:downvote: *${total}* farts :dash:`)
  })
}

module.exports = farts
