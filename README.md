![](images/jobot.jpg)

# bot-jobot
Jobot is a nasty wet slack bot

## Development

### Configure
Environment variables should be placed in `.env` for development.

| Variable                   | Value                                       |
|----------------------------|---------------------------------------------|
| HUBOT_SLACK_TOKEN          | Hubot Slack token                           |
| FIREBASE_PROJECT_ID        | Firebase project id                         |
| FIREBASE_CLIENT_EMAIL      | Firebase email                              |
| FIREBASE_PRIVATE_KEY       | Firebase private key                        |
| FIREBASE_DATABASE_URL      | Firebase database url                       |
| HUBOT_HEROKU_KEEPALIVE_URL | The URL to hit to keep the bot alive (prod) |
| REDISTOGO_URL              | Redis URL (prod)                            |

### Run
- Run the bot with `npm run dev`

### Edit
- All custom commands are in `scripts/`, organized by type

## Production
- Automatic deployment with GitLab CI upon pushing to master branch

## Usage
- Run "jobot help" or DM jobot "help" in Slack for available commands
