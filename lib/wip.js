/*
{
    "text": "Fart Leaderboard for the Past Week",
    "attachments": [
        {
      "color": "#CFB53B",
      "fields":[
        {
        "title": "First Place",
        "value": "G.K. - 9"
        }
      ]
        },
    {
      "color": "#E6E8FA",
      "fields":[
        {
        "title": "Second  Place",
        "value": "G.K. - 9"
        }
      ]
        },
    {
      "color": "#8C7853",
      "fields":[
        {
        "title": "Third  Place",
        "value": "G.K. - 9"
        }
      ]
        },
    {
      "text": "Other contestants",
      "fields":[
        {
        "title": "Third  Place",
        "value": "G.K. - 9"
        },
                {
        "title": "Third  Place",
        "value": "G.K. - 9"
        },
                {
        "title": "Third  Place",
        "value": "G.K. - 9"
        }
      ]
        }
    ]
}
  */

function formatLeaderboardForSlack (totals, type) {
  const payload = {}
  const attachments = []
  type = type.replace(/^\w/, c => c.toUpperCase())
  payload.text = `Fart Leaderboard for the Past ${type}`

  if (totals.length > 0) {
    const sorted = totals.sort((a, b) => {
      if (a.result < b.result) {
        return -1
      } else if (a.result === b.result) {
        return 0
      } else {
        return 1
      }
    })

    // First Place
    attachments.push({
      color: '#CFB53B',
      fields: [
        {
          title: 'First Place',
          value: `${sorted[0].user}. - ${sorted[0].result}`
        }
      ]
    })

    if (sorted.length > 1) {
      // Second Place
      const total = sorted[1]
      attachments.push({
        color: '#E6E8FA',
        fields: [
          {
            title: 'Second Place',
            value: `${total.user} - ${total.result}`
          }
        ]
      })
    }

    if (sorted.length > 2) {
      // Third Place
      const total = sorted[2]
      attachments.push({
        color: '#8C7853',
        fields: [
          {
            title: 'Third Place',
            value: `${total.user} - ${total.result}`
          }
        ]
      })
    }

    if (sorted.length > 3) {
      // Other
      const remainder = sorted.slice(3)
      const attachment = {
        text: 'Other contestants',
        fields: remainder.map(c => {
          return {
            value: `${c.user} - ${c.result}`
          }
        })
      }
      attachments.push(attachment)
    }
  } else {
    // No Contestants
    attachments.push({
      text: `No contestants for this ${type}`
    })
  }
  payload.attachments = attachments

  return payload
}
module.export = formatLeaderboardForSlack
